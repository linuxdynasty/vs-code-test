# Welcome to FiboWeb
The Fibonacci Sequence Generator

## API Examples

1. To fetch the 1st 5 numbers in the Fibonacci sequence http://127.0.0.1:8000/?n=5
1. To validate that 21 is a valid Fibonacci number and fetch the Fibonacci sequence until the last 2 numbers == 21 http://127.0.0.1:8000/?n=21&v=yes

### Parameter Arguments
1. `n` by itself is the index that the Fibonacci sequence generator will generate starting at 0.
2. `v` is the validator option. `v` with `n` changes on how this API behaves. `n` instead of being an index is now the number to count up to until the last numbers == `n`

### Returns

1. If api is successful, it will return a sequence of numbers that lead into the number inputted, if the number is a valid Fibonacci number.
2. If the argument passed is not a valid Fibonacci number it will raise a `400`
3. If no arguments are passed it will raise a `400`

## Prerequistes to run fiboweb

1. Git installed
2. Python installed ( 2.7.x or 3.7.x )
2. clone fiboweb repo `https://linuxdynasty@bitbucket.org/linuxdynasty/vs-code-test.git`
3. (Optional) Docker installed

## Prerequistes to run fiboweb tests

1. Python installed ( 2.7.x or 3.7.x )
2. `sudo pip install  -r requirements.txt`

## Building and running from Docker

2. cd into cloned repo. `cd vs-code-test`
3. Build docker image. `docker build -t fiboweb-alpine .`
4. Run docker container `docker run -d -it -p 8000 fiboweb-alpine`

## Building and running outside of a container

1. `cd vs-code-test`
2. `sudo pip install -r requirements.txt`
3. `gunicorn fiboweb.fiboweb:api`

## Building and running tests

1. `cd vs-code-test`
2. `sudo pip install -r requirements.txt`
6. `tox`
