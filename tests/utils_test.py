#!/usr/bin/env python

from __future__ import absolute_import

from fiboweb.utils import (
    validate_and_convert_input, fetch_fib_sequence, is_perfect_square,
    is_fib, validate_fib, fibonacci_gen_on_index, fibonacci_gen_on_number,
    empty
)

def test_is_perfect_square():
    is_perfect = (
        is_perfect_square(5 * 8 * 8 + 4) or
        is_perfect_square(5 * 8 * 8 - 4)
    )
    assert is_perfect == True

def test_is_not_perfect_square():
    is_perfect = (
        is_perfect_square(5 * 4 * 4 + 4) or
        is_perfect_square(5 * 4 * 4 - 4)
    )
    assert is_perfect == False

def test_valid_is_fib():
    is_true_fib = is_fib(5)
    assert is_true_fib == True

def test_invalid_is_fib():
    is_false_fib = is_fib(4)
    assert is_false_fib == False

def test_wrapper_valid_is_fib():
    is_true_fib, msg = validate_fib(5)
    assert is_true_fib == True
    assert msg == "5 is a valid Fibonacci number"

def test_wrapper_invalid_is_fib():
    is_false_fib, msg = validate_fib(4)
    assert is_false_fib == False
    assert msg == "4 is not a valid Fibonacci number"

def test_valid_string_int():
    num, valid_num, msg = validate_and_convert_input("5")
    assert valid_num == True
    assert isinstance(num, int)

def test_valid_int():
    num, valid_num, msg = validate_and_convert_input(8)
    assert valid_num == True
    assert isinstance(num, int)

def test_invalid_string_float():
    num, valid_num, msg = validate_and_convert_input("22.2")
    assert valid_num == False
    assert isinstance(num, str)

def test_invalid_float():
    num, valid_num, msg = validate_and_convert_input(33.3)
    assert valid_num == False
    assert isinstance(num, float)

def test_invalid_string():
    num, valid_num, msg = validate_and_convert_input("FooBar")
    assert valid_num == False
    assert isinstance(num, str)

def test_invalid_malicious_string():
    num, valid_num, msg = validate_and_convert_input("eval(while)")
    assert valid_num == False
    assert isinstance(num, str)

def test_fibonacci_gen_on_index():
    fibgen = fibonacci_gen_on_index(5)
    fiblist = list(fibgen)
    assert fiblist == [0,1,1,2,3]
    assert fiblist[-1] + fiblist[-2] == 5

def test_fibonacci_gen_on_num():
    fibgen = fibonacci_gen_on_number(13)
    fiblist = list(fibgen)
    assert fiblist == [0,1,1,2,3,5,8]
    assert fiblist[-1] + fiblist[-2] == 13

def test_fib_sequence_one_on_index():
    fibgen, all_good, msg = fetch_fib_sequence(7, on_index=True, on_num=False)
    fiblist = list(fibgen)
    assert all_good == True
    assert fiblist == [0,1,1,2,3,5,8]
    assert fiblist[-1] + fiblist[-2] == 13

def test_fib_sequence_one_on_num():
    fibgen, all_good, msg = fetch_fib_sequence(8, on_index=False, on_num=True)
    fiblist = list(fibgen)
    assert all_good == True
    assert fiblist == [0,1,1,2,3,5]
    assert fiblist[-1] + fiblist[-2] == 8

def test_fib_sequence_one_on_invalid_num():
    fibgen, all_good, msg = fetch_fib_sequence(7, on_index=False, on_num=True)
    fiblist = list(fibgen)
    assert all_good == False
    assert fiblist == [None]
    assert msg == "7 is not a valid Fibonacci number"

def test_negative_fib_sequence_one():
    fibgen, all_good, msg = fetch_fib_sequence(-4)
    assert all_good == False

def test_empty_generator():
    empty_gen = empty()
    assert list(empty_gen) == [None]
