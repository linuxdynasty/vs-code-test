#!/usr/bin/env python

from __future__ import absolute_import
from falcon import testing

from fiboweb import fiboweb

class FiboWebTestCase(testing.TestCase):
    def setUp(self):
        super(FiboWebTestCase, self).setUp()
        self.api = fiboweb.start()


class TestFiboWeb(FiboWebTestCase):
    def test_fib_index(self):
        result = self.simulate_get(params={'n': '5'})
        self.assertEqual(result.json, [0, 1, 1, 2, 3])

    def test_valid_fib_num(self):
        result = self.simulate_get(params={'n': '5', 'v': 'yes'})
        self.assertEqual(result.json, [0, 1, 1, 2, 3])

    def test_invalid_fib_num(self):
        result = self.simulate_get(params={'n': '7', 'v': 'yes'})
        self.assertEqual(result.status_code, 400)

    def test_negative_fib(self):
        result = self.simulate_get(params={'n': '-4'})
        self.assertEqual(result.status_code, 400)

    def test_invalid_string_fib(self):
        result = self.simulate_get(params={'n': 'WoooHaaaaaa'})
        self.assertEqual(result.status_code, 400)

    def test_without_parameters(self):
        result = self.simulate_get()
        self.assertEqual(result.status_code, 400)
