FROM alpine:3.7

ADD requirements.txt .
RUN apk add --update musl python3 py-pip && \
    rm /var/cache/apk/*

RUN pip install -r requirements.txt
COPY ./fiboweb fiboweb

CMD ["gunicorn", "-b", "0.0.0.0:8000", "-w", "4", "fiboweb.fiboweb:api"]
EXPOSE 8000