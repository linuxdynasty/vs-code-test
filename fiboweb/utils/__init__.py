#!/usr/bin/env python

from math import sqrt

def empty():
    """ Python 2.7 Hack to Return an Empty Generator
    """
    yield

def is_perfect_square(num):
    """ Return True if num is a perfect square

    Args:
        num(int): The number to calculate the perfect square

    Example:
        >>> is_perfect_square(5)
        True

    Returns:
        Bool

    """
    num_sqrt = int(sqrt(num))
    perfect = (num_sqrt * num_sqrt == num)
    return perfect

def is_fib(num):
    """ Return True if num is a valid Fibonacci number

    Args:
        num(int): The fibonacci number to be validated

    Examples:
        >>> is_fib(5)
        True

    Returns:
        Bool

    Note:
        num is a valid Fibonacci if one of 5 * num * num + 4 or 5 * num * num - 4
        or both is a perfect square. reference below::
        https://en.wikipedia.org/wiki/Fibonacci_number#Recognizing_Fibonacci_numbers

    """
    fib = (
        is_perfect_square(5 * num * num + 4) or
        is_perfect_square(5 * num * num - 4)
    )
    return fib

def validate_fib(num):
    """ Validate if number is a valid Fibonacci number

    Args:
        num(int): The fibonacci number to be validated

    Example:
        >>> validate_fib(5)
        (True, '5 is a valid Fibonacci number')

    Returns:
        Tuple (bool, str)

    """
    msg = "{0} is a valid Fibonacci number".format(num)
    validated = is_fib(num)
    if not validated:
        msg = "{0} is not a valid Fibonacci number".format(num)

    return (validated, msg)

def validate_and_convert_input(num):
    """ Validate if integer is a valid number or if string can
        be converted into a valid integer.

    Args:
        num(int): The fibonacci number to be validated

    Example:
        >>> validate_and_convert_input(5)
        (5, True, '5 is a valid number')

    Returns:
        Tuple (int, bool, bool, str)

    """
    valid_num = False
    msg = '{0} is a valid number'.format(num)
    err_msg = '{0} could not be converted to a valid Integer'.format(num)
    if isinstance(num, str):
        try:
            new_num = eval(num)
            if isinstance(new_num, int):
                num = new_num
                valid_num = True
            else:
                msg = err_msg

        except (SyntaxError, ValueError, NameError):
            msg = err_msg

    elif not isinstance(num, int):
        msg = "Something Nefarious is happening here on :{0}".format(num)

    elif isinstance(num, int):
        valid_num = True

    return (num, valid_num, msg)

def fibonacci_gen_on_index(num):
    """Fibonacci numbers generator until index

    Args:
        num(int): The number of iterations to count until you reach num.

    Example:
        >>> print([i for i in fibonacci_gen_on_index(5)])
        [0, 1, 1, 2, 3]

    Yields:
        int: The next number in the fibonacci sequence until num is reached

    """
    current_num, next_num, counter = 0, 1, 0
    while (counter < num):
        yield current_num
        current_num, next_num = next_num, current_num + next_num
        counter += 1

def fibonacci_gen_on_number(num):
    """Fibonacci numbers generator until the last 2 numbers is == num

    Args:
        num(int): The number of iterations to count until you reach num.

    Example:
        >>> print([i for i in fibonacci_gen_on_number(5)])
        [0, 1, 1, 2, 3]

    Yields:
        int: The next number in the fibonacci sequence until num is reached

    """
    current_num, next_num, = 0, 1
    while (next_num <= num):
        yield current_num
        current_num, next_num = next_num, current_num + next_num


def fetch_fib_sequence(num, on_index=True, on_num=False):
    """ Return the Fibonacci sequence

    Args:
        num(int): The fibonacci index number

    Kwargs:
        on_index(bool): Default=True - Fetch Fibonacci Sequence until index is
            reached.
        on_num(bool): Default=False - Fetch Fibonacci Sequence until num is
            reached.

    Example:
        >>> fetch_fib_sequence_on_index(5)
        ( <generator object fibonacci at 0x7f5f2943b460>, True, '5 is a valid Fibonacci number' )

    Returns:
        Tuple (generator object, bool, str)

    """

    msg = ""
    all_good = True
    next_fib = empty()
    num, is_valid_num, msg = validate_and_convert_input(num)
    if is_valid_num:
        if num < 0:
            msg = "FiboWeb doesn't support negative numbers {0}".format(num)
            all_good = False
        else:
            if on_index and not on_num:
                next_fib = fibonacci_gen_on_index(num)
            elif on_num and not on_index:
                valid_fib, msg = validate_fib(num)
                if valid_fib:
                    next_fib = fibonacci_gen_on_number(num)
                else:
                    all_good = False

    else:
        all_good = False

    return(next_fib, all_good, msg)
