#!/usr/bin/env python

from __future__ import absolute_import
from json import dumps
import falcon

from .utils import fetch_fib_sequence, validate_fib


class Fib(object):
    def on_get(self, req, resp):
        """Handles GET requests for the Fibonacci Sequence Generator"""
        on_index = True
        on_num = False
        fib_num = req.get_param('n')
        validate = req.get_param('v')
        if validate:
            on_index = False
            on_num = True
        if fib_num:
            fib_sequence, all_good, msg = (
                fetch_fib_sequence(fib_num, on_index, on_num)
            )
            if all_good:
                resp.body = dumps(([i for i in fib_sequence]), indent=4)
            else:
                raise falcon.HTTPInvalidParam(msg, "n={0}".format(fib_num))
        else:
            raise falcon.HTTPMissingParam("n")

def start():
    """ Start the Falcon API

    Returns:
        Falcon.API
    """
    api = falcon.API()
    fib = Fib()
    api.add_route('/', fib)
    return api

api = start()
